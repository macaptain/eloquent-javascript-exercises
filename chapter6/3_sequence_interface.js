// Eh, this is quite bad... At least it's not (too) stateful.

// Interface
// head() - return the current value
// tail() - return the tail of the sequence

function logFive(seq) {
  var currentSeq = seq;
  for (var i = 0; i < 5; i++) {
    var tail = currentSeq.tail();
    if (tail === null) {
      return;
    }
    console.log(currentSeq.head());
    currentSeq = tail;
  }
}

function ArraySeq(array) {
  this.values = array;
}

ArraySeq.prototype.head = function() {
  if (this.values.length === 0) {
    return null;
  }
  return this.values[0];
};

ArraySeq.prototype.tail = function() {
  if (this.values.length === 0) {
    return null;
  }
  return new ArraySeq(this.values.slice(1));
};

function range(from, to) {
  var range = [];
  for (var i = from; i < to; i++) {
    range.push(i);
  }
  return range;
}

function RangeSeq(from, to) {
  this.values = range(from, to);
}

RangeSeq.prototype.tail = function() {
  if (this.values.length === 0) {
    return null;
  }
  return new RangeSeq(this.values[1], this.values[this.values.length - 1]);
};

// Can inherit, but we're storing a lot more data than we need to!
RangeSeq.prototype = Object.create(ArraySeq.prototype);

logFive(new ArraySeq([1, 2]));
// 1
// 2
logFive(new RangeSeq(100, 1000));
// 100
// 101
// 102
// 103
// 104
