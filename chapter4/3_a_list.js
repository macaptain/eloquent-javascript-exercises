/**
 * Add a new list with a given value which links the given list.
 * @param {object} elt Value of new element in new list
 * @param {list} list List to be linked to
 * @return {list} A new list which links to the given list with the given value
 */
function prepend(elt, list) {
  return {
    value: elt,
    rest: list
  };
}

/**
 * Get the nth item of a linked list
 * @param  {list} inputList linked list
 * @param  {number} n index of the item
 * @return {list} list
 */
function nth(inputList, n) {
  if (!inputList) {
    return undefined;
  }
  if (n === 0) {
    return inputList.value;
  }
  return nth(inputList.rest, n - 1);
}

/**
 * Convert an array to a linked list
 * @param  {Array} array Any array of any types
 * @return {List} Linked list with value set to the values of the array
 */
function arrayToList(array) {
  // Start with null and recurse back
  var list = null;
  for (var i = array.length - 1; i >= 0; i--) {
    list = prepend(array[i], list);
  }
  return list;
}

/**
 * Convert a linked list to an array of values
 * @param  {list} inputList Linked list
 * @return {Array} Array of values
 */
function listToArray(inputList) {
  var array = [];
  // instead of while, use 'for' with 'list.rest' iterator
  while (inputList) {
    array.push(inputList.value);
    inputList = inputList.rest;
  }
  return array;
}

console.log(arrayToList([10, 20]));
// {value: 10, rest: {value: 20, rest: null}}
console.log(listToArray(arrayToList([10, 20, 30])));
// [10, 20, 30]
console.log(prepend(10, prepend(20, null)));
// {value: 10, rest: {value: 20, rest: null}}
console.log(nth(arrayToList([10, 20, 30]), 1));
// 20
