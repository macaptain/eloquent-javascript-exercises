/**
 * Return true if and only if two values or object are equal in all values and
 * properties.
 * @param  {Object} x Object or value
 * @param  {Object} y [description]
 * @return {Boolean}   [description]
 */
function deepEqual(x, y) {
  if (x === y) {
    return true;
  }

  // Do a null check here before accessing properties!
  if (x === null || typeof x !== 'object' ||
  y === null || typeof x !== 'object') {
    return false;
  }

  // It's sufficient to check that the two objects have the same number of
  // properties and that properties are equal to the other.
  var numPropsX = Object.keys(x).length;
  var numPropsY = Object.keys(y).length;

  if (numPropsX !== numPropsY) {
    return false;
  }

  for (var key in x) {
    if (!y[key] || !deepEqual(x[key], y[key])) {
      return false;
    }
  }
  return true;
}

var obj = {here: {is: 'an'}, object: 2};
console.log(deepEqual(obj, obj));
// true
console.log(deepEqual(obj, {here: 1, object: 2}));
// false
console.log(deepEqual(obj, {here: {is: 'an'}, object: 2}));
// true
