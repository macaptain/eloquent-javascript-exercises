/**
 * Returns an array for the range between two numbers
 * @param  {number} start Where the range begins (inclusive)
 * @param  {number} end   Where the range ends (inclusive)
 * @param  {number} step  Number by which to increment elements of the range
 * @return {Array.<number>} Array of integers between the start and end
 */
function range(start, end, step) {
  if (step === undefined) {
    step = 1;
  }
  if (step === 0) {
    return [];
  }
  var rangeArray = [];

  /**
   * Determine if a number i has exceeding the end bound of an increasing or
   * decreasing sequence.
   * @param  {integer}  i            [description]
   * @param  {Boolean} isIncreasing True if the sequence is increasing
   * @return {Boolean}               True if and only if i is in the bound
   */
  function indexBeyondRange(i, isIncreasing) {
    if (isIncreasing) {
      return i <= end;
    }
    return i >= end;
  }
  for (var i = start; indexBeyondRange(i, step > 0); i += step) {
    rangeArray.push(i);
  }
  return rangeArray;
}

/**
 * Return the sum of an array of numbers
 * @param  {Array.<number>} xs Array of numbers
 * @return {number}    Sum of the elements of xs
 */
function sum(xs) {
  var total = 0;
  for (var i = 0; i < xs.length; i++) {
    total += xs[i];
  }
  return total;
}

console.log(range(1, 10, 1));
console.log(range(5, 2, -1));
console.log(sum(range(1, 10)));
