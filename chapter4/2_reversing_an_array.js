/**
 * Return a new array with the elements of the first reversed
 * @param  {Array} array Input array
 * @return {Array}       New array, elements in reverse order of input array
 */
function reverseArray(array) {
  var newArray = [];
  for (var i = array.length - 1; i >= 0; i--) {
    newArray.push(array[i]);
  }
  return newArray;
}

/**
 * Return the same array reversed
 * @param  {Array} array Input array
 * @return {Array}       Input array edited in place to have elements reversed
 */
function reverseArrayInPlace(array) {
  for (var i = 0; Math.floor(i < array.length / 2); i++) {
    var old = array[i];
    array[i] = array[array.length - i - 1];
    array[array.length - i - 1] = old;
  }
  return array;
}

console.log(reverseArray([1, 2, 3, 4, 5]));
console.log(reverseArrayInPlace([1, 2, 3, 4, 5]));
