/**
 * Flatten an array of arrays into an array containing all the elements
 * @param  {Array} arrays An array of arrays
 * @return {Array}        An array containing all the elements of the arrays
 */
function flatten(arrays) {
  return arrays.reduce(function(a, b) {
    return a.concat(b);
  }, []);
}

var arrays = [[1, 2, 3], [4, 5], [6]];
console.log(flatten(arrays));
// [1, 2, 3, 4, 5, 6]
