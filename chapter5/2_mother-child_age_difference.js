
var ancestry = JSON.parse(ANCESTRY_FILE);

function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

var byName = {};
ancestry.forEach(function(person) {
  byName[person.name] = person;
});

function hasKnownMother(p) {
  return p.mother !== null && p.mother in byName;
}

function ageDifferenceFromMother(person) {
  var mother = byName[person.mother];
  return person.born - mother.born;
}

var averageAge = average(ancestry.filter(hasKnownMother)
                                 .map(ageDifferenceFromMother));
console.log(averageAge);
// 31.2
