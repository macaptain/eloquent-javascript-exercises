var ancestry = JSON.parse(ANCESTRY_FILE);

function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

function ageOfDeath(person) {
  return person.died - person.born;
}

function groupBy(array, func) {
  var groups = {};
  array.forEach(function(o) {
    var fo = func(o);
    if (fo in groups) {
      groups[fo].push(o);
    } else {
      groups[fo] = [o];
    }
  });
  return groups;
}

var centuriesToPeople = groupBy(ancestry, function(person) {
  return Math.ceil(person.died / 100);
});

for (var century in centuriesToPeople) {
  if (centuriesToPeople.hasOwnProperty(century)) {
    var avgAge = average(centuriesToPeople[century].map(ageOfDeath));
    console.log(century + ': ' + avgAge.toString());
  }
}
