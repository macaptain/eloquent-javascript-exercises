function every(array, func) {
  return array.map(func).reduce(function(a, b) {
    return a && b;
  }, true);
}

function some(array, func) {
  return array.map(func).reduce(function(a, b) {
    return a || b;
  }, false);
}

console.log(every([NaN, NaN, NaN], isNaN));
// true
console.log(every([NaN, NaN, 4], isNaN));
// false
console.log(some([NaN, 3, 4], isNaN));
// true
console.log(some([2, 3, 4], isNaN));
// false
