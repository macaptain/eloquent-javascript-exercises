var size = 8;

var grid = '';
for (var i = 0; i < size; i++) {
  for (var j = 0; j < size; j++) {
    if (i % 2 === j % 2) {
      grid += ' ';
    } else {
      grid += '#';
    }
  }
  grid += '\n';
}

console.log(grid);
