/**
 * Return the number of uppercase B's in a string
 * @param  {string} s A string
 * @return {number}   Number of 'B's in the string
 */
function countBs(s) {
  return countChar(s, 'B');
}

/**
 * Return number of instances of a character char in a string
 * @param  {string} s    A string
 * @param  {character} char A character
 * @return {number}      Number of instances
 */
function countChar(s, char) {
  var count = 0;
  for (var i = 0; i < s.length; i++) {
    if (s.charAt(i) === char) {
      count += 1;
    }
  }
  return count;
}

console.log(countBs('BBC'));
