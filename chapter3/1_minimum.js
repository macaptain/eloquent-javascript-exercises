/**
 * Take the minimum of two numbers.
 * @param  {int} a Input int.
 * @param  {int} b Input int.
 * @return {bool} The lesser of a and b.
 */
function min(a, b) {
  if (a < b) {
    return a;
  }
  return b;
}

console.log(min(0, 10));
console.log(min(0, -10));
