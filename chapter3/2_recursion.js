/**
 * Determine if a number is even.
 * @param  {Integer}  n Any integer
 * @return {Boolean}  True if n is even, false otherwise
 */
function isEven(n) {
  if (n === 0) {
    return true;
  }
  if (n === 1) {
    return false;
  }
  if (n < 0) {
    return isEven(-n);
  }
  return isEven(n - 2);
}

console.log(isEven(-1));
