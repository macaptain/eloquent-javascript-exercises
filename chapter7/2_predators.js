function Tiger() {
  this.energy = 100;
  this.hunger = 0;
}

Tiger.prototype.act = function(view) {
  var space = view.find(' ');
  if (this.energy > 110 && space) {
    this.hunger += 1;
    return {type: 'reproduce', direction: space};
  }
  var plantEater = view.find('O');
  if (plantEater && this.hunger > 5) {
    this.hunger = 0;
    return {type: 'eat', direction: plantEater};
  }
  if (space) {
    this.hunger += 1;
    return {type: 'move', direction: space};
  }
}
