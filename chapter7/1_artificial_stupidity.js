function SmartPlantEater() {
  this.energy = 20;
  this.hunger = 0;
}
SmartPlantEater.prototype.act = function(view) {
  var space = view.find(' ');
  if (this.energy > 60 && space) {
    this.hunger += 1;
    return {type: 'reproduce', direction: space};
  }
  var plant = view.find('*');
  if (plant && this.hunger > 2) {
    this.hunger = 0;
    return {type: 'eat', direction: plant};
  }
  if (space) {
    this.hunger += 1;
    return {type: 'move', direction: space};
  }
};
